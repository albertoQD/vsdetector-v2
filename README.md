### Requirements
    - Docker
    - XQuartz (for MacOS only)

### Building the docker image

You can either copy the Dockerfile in `docker/dnn_test/` or clone the whole repository to get the file; either way, later you do:

```
cd docker/dnn_test && \
docker -t vsdetectot:v2 .
```

### Running the docker image

In MacOS:

```
open -a XQuartz && \
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}') && \
xhost + $ip && \
docker run -ti -v /path/to/data:/data -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$ip:0 vsdetector:v2
```

In Linux just ignore the XQuartz command.

Be sure to change `/path/to/data/` in the previous command.
