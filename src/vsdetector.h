#ifndef VS_DETECTOR_HPP
#define VS_DETECTOR_HPP

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/saliency.hpp>
#include <vector>
#include <string>
#include <array>
#include <exception>

#include "vsobject.h"

const std::vector<std::string> VS_CLASSES = {"background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"};

class VSDetector 
{
public:
    VSDetector(const std::string &t_model_proto, const std::string &t_model_weights, float t_threshold_detection = 0.5f);
    ~VSDetector(void);

    std::vector<VSObjectData> process_frame(const cv::Mat &image_in);
    std::vector<VSObjectData> process_image(const std::string &t_image_fn); 
    std::vector<VSObjectData> process_video(const std::string &t_video_fn);

protected:
    float main_object_score(float t_score_detection, const std::array<int, 4> &t_bbox, const cv::Mat &t_saliency_map, float t_saliency_total);

private:
    cv::Ptr<cv::saliency::StaticSaliencySpectralResidual> m_saliency_algo;
    float m_threshold_detection;
    cv::dnn::Net m_net;
    
};

#endif