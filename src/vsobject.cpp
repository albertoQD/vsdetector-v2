#include "vsobject.h"


VSObjectData::VSObjectData(int t_label_idx, 
                        const std::string &t_label,
                        const std::array<int, 4> &t_bbox,
                        float t_score_main,
                        const cv::Mat &t_ref_frame) :
    m_counter_appearances(1),
    m_counter_main_object(0)
{
    m_label = t_label;
    m_label_idx = t_label_idx;
    m_bounding_box = t_bbox;
    m_score_main = t_score_main;
    m_ref_frame = t_ref_frame.clone();
}

VSObjectData::~VSObjectData(void)
{
}

void VSObjectData::increase_appearances(void)
{
    m_counter_appearances += 1;
}

int VSObjectData::appearances(void) const 
{
    return m_counter_appearances;
}

void VSObjectData::increase_main_in_file(void)
{
    m_counter_main_object += 1;
}

int VSObjectData::main_in_file(void) const
{
    return m_counter_main_object;
}

int VSObjectData::label_idx(void) const
{
    return m_label_idx;
}

float VSObjectData::score_main(void) const
{
    return m_score_main;
}

std::string VSObjectData::label(void) const
{
    return m_label;
}

void VSObjectData::score_main(float t_score)
{
    m_score_main = t_score;
}

void VSObjectData::ref_frame(const cv::Mat &t_ref_frame)
{
    m_ref_frame = t_ref_frame.clone();
}

cv::Mat VSObjectData::ref_frame(void) const
{
    return m_ref_frame;
}

void VSObjectData::bounding_box(const std::array<int,4> &t_bbox)
{
    m_bounding_box = t_bbox;
}

std::array<int,4> VSObjectData::bounding_box(void) const
{
    return m_bounding_box;
}