#include "vsdetector.h"

VSDetector::~VSDetector(void)
{
}

VSDetector::VSDetector(const std::string &t_model_proto, const std::string &t_model_weights, float t_threshold_detection) :
    m_threshold_detection(t_threshold_detection)
{
    m_net = cv::dnn::readNetFromCaffe(t_model_proto, t_model_weights);

    if (m_net.empty())
    {
        throw std::runtime_error("Could not load the caffe model");
    }

    m_saliency_algo = cv::saliency::StaticSaliencySpectralResidual::create();
}


std::vector<VSObjectData> VSDetector::process_image(const std::string &t_image_fn)
{
    cv::Mat image_in = cv::imread(t_image_fn);
    if (image_in.empty())
    {
        std::cerr << " :: Error :: Could not read the input image" << std::endl;
        return std::vector<VSObjectData>();
    }

    return process_frame(image_in);
}

std::vector<VSObjectData> VSDetector::process_frame(const cv::Mat &image_in)
{
    cv::Size size_original = image_in.size();

    // compute saliency first
    cv::Mat saliency_map;
    m_saliency_algo->computeSaliency(image_in, saliency_map);
    float saliency_total = 0.0f;
    for (int x=0; x<saliency_map.cols; x+=1)
    {
        for (int y=0; y<saliency_map.rows; y+=1)
        {
            saliency_total += saliency_map.at<float>(y,x);
        }
    }
    
    cv::Mat blob_in = cv::dnn::blobFromImage(image_in, 0.007843, cv::Size(300, 300), cv::Scalar(127));
    m_net.setInput(blob_in, "data");
    
    cv::Mat blob_out = m_net.forward();
    int detections = blob_out.size[2];

    std::vector<VSObjectData> local_objects;
    for (int i=0; i<detections; i++)
    {
        cv::Vec<float, 7> detected = *(blob_out.ptr<cv::Vec<float,7>>(0)+i);
        int label_idx = detected[1]; 
        float label_score = detected[2];
        if (label_score < m_threshold_detection)
        {
            continue;
        }
        std::string label = VS_CLASSES[label_idx];
        // computing bbox
        int x_init = detected[3] * size_original.width;
        int y_init = detected[4] * size_original.height;
        int x_end = detected[5] * size_original.width;
        int y_end = detected[6] * size_original.height;

        // compute the score of the object to be main 
        // object on the image
        float score_main = main_object_score(label_score, {x_init, y_init, x_end, y_end}, saliency_map, saliency_total);

        // add object to local_objects
        local_objects.push_back(VSObjectData(label_idx, label, {x_init, y_init, x_end, y_end}, score_main, image_in));
    }

    // sort local_objects by main_score 
    std::sort(begin(local_objects), end(local_objects), [](const VSObjectData &o1, const VSObjectData &o2) -> bool {
        return o1.score_main() > o2.score_main();
    });

    return local_objects;
}

std::vector<VSObjectData> VSDetector::process_video(const std::string &t_video_fn)
{
    std::vector<VSObjectData> detected_objs; 
    std::vector<int> found(21, 0);

    // Open the video
    cv::VideoCapture cap(t_video_fn);
    if (!cap.isOpened())
    {
        std::cerr << " :: ERROR :: Could not open the file: " << t_video_fn << std::endl;
        return std::vector<VSObjectData>();
    }
    
    cv::Mat frame_image;
    // get FPS to know how many frames to drop
    int video_fps = (int) cap.get(CV_CAP_PROP_FPS);
    video_fps = (video_fps == 0) ? 30 : video_fps;

    int frames_count = (int) cap.get(CV_CAP_PROP_FRAME_COUNT);

    int current_frame = 0;
    cap.set(CV_CAP_PROP_POS_FRAMES, (double) current_frame);

    while(1) 
    {
        std::cout << " :: Info :: Processing frames " << current_frame << " / " << frames_count << " \r" << std::flush;
        cap >> frame_image;
        if (frame_image.empty()) break;

        auto frame_objs = process_frame(frame_image);

        // for every found obj
        for (auto obj : frame_objs)
        {
            int obj_idx = found[obj.label_idx()];
            if ( obj_idx > 0)
            { // it already exists
                detected_objs[obj_idx].increase_appearances();
                if (detected_objs[obj_idx].score_main() < obj.score_main())
                {
                    detected_objs[obj_idx].score_main(obj.score_main());
                    detected_objs[obj_idx].ref_frame(obj.ref_frame());
                    detected_objs[obj_idx].bounding_box(obj.bounding_box());
                }
            }
            else
            {
                found[obj.label_idx()] = detected_objs.size();
                detected_objs.push_back(obj); 
            }
        }

        if (frame_objs.size()>0)
        {
            // increase now the main_in_file
            detected_objs[found[frame_objs[0].label_idx()]].increase_main_in_file();
        }

        current_frame += video_fps;
        cap.set(CV_CAP_PROP_POS_FRAMES, (double) current_frame);
    }

    // sort for all objs
    std::sort(begin(detected_objs), end(detected_objs), [](const VSObjectData &o1, const VSObjectData &o2) -> bool {
        return o1.main_in_file() > o2.main_in_file();
    });

    return detected_objs;
}

float VSDetector::main_object_score(float t_score_detection, const std::array<int, 4> &t_bbox, const cv::Mat &t_saliency_map, float t_saliency_total)
{
    /********************************************************
     * Ss = Sum of saliency in the bbox (normalized)
     * Ab = Area of the bbox (normalized)
     * Dc = Distance from center of bbox to the center of the image (normalized)
     * Sd = Score of the detection
     *
     * Final Score = Ss + Ab + (1-Dc) + Sd 
     **********************************************************/

    // sum of saliency in bbox
    float saliency_sum = 0.0f;
    for (int x=t_bbox[0]; x<t_bbox[2]; x+=1)
    {
        for (int y=t_bbox[1]; y<t_bbox[3]; y+=1)
        {
            saliency_sum += t_saliency_map.at<float>(y,x);
        }
    }
    saliency_sum /= t_saliency_total;
    // area of bbox
    int width = t_bbox[2]-t_bbox[0];
    int height = t_bbox[3]-t_bbox[1];
    float bbox_area = float(height*width)/float(t_saliency_map.rows*t_saliency_map.cols);
    // distance to center (assuming saliency_map is 
    // the same size as the input image)
    cv::Point2f center_image(0.5f, 0.5f);
    cv::Point2f center_bbox((t_bbox[0]+width/2)/t_saliency_map.cols, 
                            (t_bbox[1]+height/2)/t_saliency_map.rows);
    float diff_x = center_image.x - center_bbox.x;
    float diff_y = center_image.y - center_bbox.y;
    float distance_to_center = 1 - std::sqrt(diff_x*diff_x + diff_y*diff_y);

    // std::cout << "------------------------------------" << std::endl;
    // std::cout << "        saliency_sum: " << saliency_sum << std::endl;
    // std::cout << "  distance_to_center: " << distance_to_center << std::endl;
    // std::cout << "           bbox_area: " << bbox_area << std::endl;
    // std::cout << "     detection_score: " << t_score_detection << std::endl;


    return saliency_sum + distance_to_center + bbox_area + t_score_detection;
}
