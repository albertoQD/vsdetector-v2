#include "vsdetector.hpp"
#include <iostream>

int main(int, char **)
{
    std::string image_in_filename = "../data/example_03.jpg";
    std::string video_in_filename = "../data/horse_runs_over_car.avi";

    VSDetector le_detector("../models/MobileNetSSD_deploy.prototxt","../models/MobileNetSSD_deploy.caffemodel");

    // auto objs = le_detector.process_image(image_in_filename);
    // for (const auto &obj : objs)
    // {
    //     std::cout << "-----------------------------------" << std::endl;
    //     std::cout << "         label: " << obj.label() << std::endl;
    //     std::cout << "    score_main: " << obj.score_main() << std::endl;
    //     std::cout << "   appearances: " << obj.appearances() << std::endl;
    // }

    auto objs = le_detector.process_video(video_in_filename);

    for (const auto &obj : objs)
    {
        std::cout << "-----------------------------------" << std::endl;
        std::cout << "           label: " << obj.label() << std::endl;
        std::cout << "     appearances: " << obj.appearances() << std::endl;
        std::cout << "    main_in_file: " << obj.main_in_file() << std::endl;
    }

    return 0;
}

int main2(int argc, char **argv)
{
    const std::string model_w = "../models/MobileNetSSD_deploy.caffemodel";
    const std::string model_a = "../models/MobileNetSSD_deploy.prototxt";
    std::string image_in_filename = "../data/example_05.jpg";
    std::string image_out_filename = "../data/example_05_res.jpg";

    const std::vector<std::string> CLASSES = {"background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"};

    // load Net
    cv::dnn::Net le_net = cv::dnn::readNetFromCaffe(model_a, model_w);
    if (le_net.empty())
    {
        std::cerr << " :: Error :: Could not load the caffe model" << std::endl;
        exit(0);
    }

    // read input image
    cv::Mat image_in = cv::imread(image_in_filename);
    if (image_in.empty())
    {
        std::cerr << " :: Error :: Could not read the input image" << std::endl;
        exit(0);
    }

    cv::Size size_original = image_in.size();
    
    cv::Mat blob_in = cv::dnn::blobFromImage(image_in, 0.007843, cv::Size(300, 300), cv::Scalar(127));
    le_net.setInput(blob_in, "data");
    
    cv::TickMeter tm;
    tm.start();
    cv::Mat blob_out = le_net.forward();
    tm.stop();

    std::cout << " :: Info :: Moving through the net took: " << tm.getTimeSec() << " seconds" << std::endl;

    // std::cout << " :: Info :: Blob size: [ ";
    // for(int i = 0; i < blob_out.dims; ++i) {
    //     if(i) std::cout << " x ";
    //     std::cout << blob_out.size[i];
    // }
    // std::cout << " ]" << std::endl;

    cv::RNG le_rng(0xFFFFFF);

    int detections = blob_out.size[2];
    
    std::cout << " :: Info :: { " << std::endl;
    for (int i=0; i<detections; i++)
    {
        cv::Vec<float, 7> detected = *(blob_out.ptr<cv::Vec<float,7>>(0)+i);
        int label_idx = detected[1]; 
        float label_score = detected[2];
        // computing bbox
        int x_init = detected[3] * size_original.width;
        int y_init = detected[4] * size_original.height;
        int x_end = detected[5] * size_original.width;
        int y_end = detected[6] * size_original.height;
        // draw the detected object on the image 
        int color = le_rng(256);
        // cv::rectangle(image_in, cv::Point(x_init, y_init), cv::Point(x_end, y_end), cv::Scalar(color, color, color));

        std::cout << "\t" 
                  << CLASSES[label_idx] << ": " 
                  << label_score << " [" 
                  << x_init << ", " 
                  << y_init << ", " 
                  << x_end << ", " 
                  << y_end << "]" 
                  << std::endl;
    }
    std::cout << " }" << std::endl;

    cv::imwrite(image_out_filename, image_in);


    auto saliencyAlgo1 = cv::saliency::StaticSaliencyFineGrained::create();
    cv::Mat saliency_map;
    saliencyAlgo1->computeSaliency(image_in, saliency_map);
    cv::normalize(saliency_map, saliency_map, 0, 255, cv::NORM_MINMAX);
    cv::imwrite("../data/saliency_map1.png", saliency_map);
  
    auto saliencyAlgo2 = cv::saliency::StaticSaliencySpectralResidual::create();
    saliencyAlgo2->computeSaliency(image_in, saliency_map);
    cv::Mat binary_map;
    cv::saliency::StaticSaliencySpectralResidual spec;
    spec.computeBinaryMap(saliency_map, binary_map);

    cv::normalize(saliency_map, saliency_map, 0, 255, cv::NORM_MINMAX);
    cv::imwrite("../data/saliency_map2.png", saliency_map);
    cv::imwrite("../data/binary_map.png", binary_map);

    cv::imwrite("../data/resized.png", image_in);


    return 0;
}