#ifndef VS_OBJECT_DATA_HPP
#define VS_OBJECT_DATA_HPP

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

class VSObjectData
{
public:
    VSObjectData(int t_label_idx, 
                 const std::string &t_label,
                 const std::array<int, 4> &t_bbox,
                 float t_score_main,
                 const cv::Mat &t_ref_frame);

    ~VSObjectData(void);

    void increase_appearances(void);
    int appearances(void) const;

    void increase_main_in_file(void);
    int main_in_file(void) const;
    int label_idx(void) const;
    float score_main(void) const;
    std::string label(void) const;
    void score_main(float t_score);
    void ref_frame(const cv::Mat &t_ref_frame);
    cv::Mat ref_frame(void) const;
    void bounding_box(const std::array<int,4> &t_bbox);
    std::array<int,4> bounding_box(void) const;

private:
    int m_label_idx;
    std::string m_label;
    // How many times it appears ? 
    int m_counter_appearances; 
    // of those appearences, how many times it was main object ?
    int m_counter_main_object;
    float m_score_main;
    // keep a reference to the france wher it has the biggest score
    cv::Mat m_ref_frame;
    std::array<int, 4> m_bounding_box;
};


#endif