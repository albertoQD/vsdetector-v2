#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QMessageBox>
#include <QProgressDialog>
#include <QEventLoop>
#include "mattoqimage.hpp"
#include "vsobject.h"

#define LABEL_HEIGHT 410
#define LABEL_WIDTH 490

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_init(false)
{
    ui->setupUi(this);

    m_main = new QTreeWidgetItem(ui->treeWidget);
    m_main->setText(0, "Main Object");

    m_root = new QTreeWidgetItem(ui->treeWidget);
    m_root->setText(0, "Detected Objects");

    statusBar()->showMessage(tr("Loading model ..."));
    init_model();

    ui->actionShow_detected_mask->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete m_detector;
    delete m_root;
    delete ui;
}

void MainWindow::init_model(void)
{
    std::string file_proto = "../models/MobileNetSSD_deploy.prototxt";
    std::string file_model = "../models/MobileNetSSD_deploy.caffemodel";
    m_detector = new VSDetector(file_proto, file_model, 0.89f);
    m_init = true;
    statusBar()->showMessage(tr("Loading model ... Done"));
}

void MainWindow::prepare_for_viewing(cv::Mat &t_image_io)
{
     // resize accordingly
    if (t_image_io.rows > LABEL_HEIGHT)
    {
        double ratio = (LABEL_HEIGHT-20)/double(t_image_io.rows);
        int new_width = std::floor(t_image_io.cols*ratio);
        cv::resize(t_image_io, t_image_io,
                   cv::Size(new_width, LABEL_HEIGHT),
                   0, 0, cv::INTER_CUBIC);
    }
    else if (t_image_io.cols > LABEL_WIDTH)
    {
        double ratio = (LABEL_WIDTH-20)/double(t_image_io.cols);
        int new_height = std::floor(t_image_io.rows*ratio);
        cv::resize(t_image_io, t_image_io,
                   cv::Size(LABEL_WIDTH, new_height),
                   0, 0, cv::INTER_CUBIC);
    }

    // now pad for better viewing
    int pad_height = std::max(LABEL_HEIGHT-t_image_io.rows, 0);
    int pad_width = std::max(LABEL_WIDTH-t_image_io.cols, 0);
    cv::copyMakeBorder(t_image_io, t_image_io,
                       std::floor(pad_height/2), std::floor(pad_height/2),
                       std::floor(pad_width/2), std::floor(pad_width/2),
                       cv::BORDER_CONSTANT, cv::Scalar(0));
}

void MainWindow::process_file(bool t_image)
{
    if (!m_init)
    {
        statusBar()->showMessage(tr("The model is not loaded yet. An error maybe?"));
        return;
    } 

    QString filename;
    if (t_image)
    {
        filename = QFileDialog::getOpenFileName(
            this,
            tr("Select image to open"),
            QDir::toNativeSeparators("/data"),
            tr("Images (*.jpeg *.jpg *.png")); 
    }
    else 
    {
        filename = QFileDialog::getOpenFileName(
            this,
            tr("Select video to open"),
            QDir::toNativeSeparators("/data"),
            tr("Videos (*.avi)"));
    }

    if (!filename.isEmpty())
    {
        // clean the treeview
        for (auto child : m_root->takeChildren())
        {
            m_root->removeChild(child);
        }

        for (auto child : m_main->takeChildren())
        {
            m_main->removeChild(child);
        }

        m_detected_objects = m_detector->process_video(filename.toStdString());

        if (m_detected_objects.size() > 0)
        {
            add_object_to_list(m_detected_objects[0], m_main);
        }

        for (auto object : m_detected_objects)
        {
            add_object_to_list(object, m_root);
        }

        if (t_image)
        {
            // open image
            cv::Mat image = cv::imread(filename.toStdString(), CV_LOAD_IMAGE_COLOR);
            m_current_image = image.clone();
            prepare_for_viewing(image);
            ui->mainImage->setPixmap(QPixmap::fromImage(MatToQImage(image)));
        }
    }
}

void MainWindow::on_actionLoad_image_triggered()
{
    process_file(true);
}

void MainWindow::add_object_to_list(const VSObjectData &t_object, QTreeWidgetItem * t_root)
{
    QTreeWidgetItem * tmp_child = new QTreeWidgetItem(t_root);
    tmp_child->setText(0, QString::fromStdString(t_object.label()));

    QTreeWidgetItem * score = new QTreeWidgetItem(tmp_child);
    score->setText(0, QString("Score: %1").arg(t_object.score_main()));

    QTreeWidgetItem * importance = new QTreeWidgetItem(tmp_child);
    importance->setText(0, QString("Appearances: %1").arg(t_object.appearances()));
}

void MainWindow::on_actionLoad_video_triggered()
{
    process_file(false);
}



void MainWindow::on_actionShow_detected_mask_triggered()
{

}

void MainWindow::on_actionList_of_objects_triggered()
{
    QMessageBox::information(this,"List objects",QString("List of possible objects to detect: \n\n"
                                                  "  - Person\n"
                                                  "  - Animan : {bird, cat, cow, horse, sheep}\n"
                                                  "  - Vehicle: {aeroplane, bicycle, boat, bus, car, motorbike, train}\n"
                                                  "  - Indoor: {bottle, chair, dining table, potted plant, sofa, tv/monitor"));
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"About",QString("Developed by Alberto Quintero-Delgado \nEmail: ajquinterod@gmail.com\n\n "));
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if (m_root->isSelected() || m_main->isSelected())
    {
        return;
    }
    int idx = -1;
    
    if (item->parent() == m_root)
    {
        idx = m_root->indexOfChild(item);
    }
    else
    {
        idx = m_main->indexOfChild(item);
    }

    cv::Mat original_image = m_detected_objects[idx].ref_frame().clone();
    auto bbox = m_detected_objects[idx].bounding_box();
    cv::Point p1(bbox[0], bbox[1]);
    cv::Point p2(bbox[2], bbox[3]);
    cv::rectangle(original_image, p1, p2, cv::Scalar(0,255,0));

    prepare_for_viewing(original_image);
    ui->mainImage->setPixmap(QPixmap::fromImage(MatToQImage(original_image)));
}
